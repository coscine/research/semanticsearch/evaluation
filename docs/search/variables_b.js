var searchData=
[
  ['rdf_222',['RDF',['../class_semantic_search_evaluation_1_1_uris.html#a262c2471fd3bb650cd2365fd8cfce766',1,'SemanticSearchEvaluation::Uris']]],
  ['rdf_5ftype_5flong_223',['RDF_TYPE_LONG',['../class_semantic_search_evaluation_1_1_uris.html#a71bbde5cf3d71a4917270bcf22111db2',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_224',['RDFS',['../class_semantic_search_evaluation_1_1_uris.html#a42a6c7390740314a11740d98d98dc110',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_5flabel_225',['RDFS_LABEL',['../class_semantic_search_evaluation_1_1_uris.html#a1e272be7de6ed699df2138f710b30c6d',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_5fsubclass_5fof_226',['RDFS_SUBCLASS_OF',['../class_semantic_search_evaluation_1_1_uris.html#af8cfe517d54f5df7912c8b7f0123a158',1,'SemanticSearchEvaluation::Uris']]],
  ['reindex_227',['REINDEX',['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a72a0c9518e92b591e33d80433dedc051',1,'SemanticSearchEvaluation::ApproachComparison::ElasticsearchEvaluation']]]
];

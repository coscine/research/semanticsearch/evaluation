var searchData=
[
  ['org_212',['ORG',['../class_semantic_search_evaluation_1_1_uris.html#ae6bfd60a14e567d54d9cddb66e1bef5d',1,'SemanticSearchEvaluation::Uris']]],
  ['org_5fhas_5funit_213',['ORG_HAS_UNIT',['../class_semantic_search_evaluation_1_1_uris.html#a9f4075a504d007c45b41432439c75ab0',1,'SemanticSearchEvaluation::Uris']]],
  ['org_5fmember_214',['ORG_MEMBER',['../class_semantic_search_evaluation_1_1_uris.html#ab2e24f4eecaeae63bb5dc4ca3da8d14d',1,'SemanticSearchEvaluation::Uris']]],
  ['org_5fmembership_215',['ORG_MEMBERSHIP',['../class_semantic_search_evaluation_1_1_uris.html#a8fbb2cd2e490b9dd420ae84da0f320c8',1,'SemanticSearchEvaluation::Uris']]],
  ['org_5forganization_216',['ORG_ORGANIZATION',['../class_semantic_search_evaluation_1_1_uris.html#ad8776493306d052a3f2d52f143b374d3',1,'SemanticSearchEvaluation::Uris']]],
  ['owl_217',['OWL',['../class_semantic_search_evaluation_1_1_uris.html#aad4fef06e3c4f8efd21ff010e99de407',1,'SemanticSearchEvaluation::Uris']]],
  ['owl_5fontology_218',['OWL_ONTOLOGY',['../class_semantic_search_evaluation_1_1_uris.html#a26088320f4febeb33452353bb5f8bee6',1,'SemanticSearchEvaluation::Uris']]]
];

var indexSectionsWithContent =
{
  0: "_abcdefgilmnopqrstuv",
  1: "abefgorsuv",
  2: "s",
  3: "abdefgrsuv",
  4: "cgmpqrsv",
  5: "_acdefilnoqrsu",
  6: "anpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};


var searchData=
[
  ['reindex_167',['Reindex',['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a54c6f787933dd0a0bf5676abeb7c2bf2',1,'SemanticSearchEvaluation::ApproachComparison::ElasticsearchEvaluation']]],
  ['removegraph_168',['RemoveGraph',['../class_semantic_search_evaluation_1_1_virtuoso_connector.html#a686884bb7084aa981aa60d48bd4344e8',1,'SemanticSearchEvaluation::VirtuosoConnector']]],
  ['runallapproaches_169',['RunAllApproaches',['../class_semantic_search_evaluation_1_1_approach_comparison.html#ae5eadf24b4cf5fa7a8c6ea0196397b0c',1,'SemanticSearchEvaluation::ApproachComparison']]],
  ['runempty_170',['RunEmpty',['../class_semantic_search_evaluation_1_1_approach_comparison.html#a0aad4979e2e91d45b8edf2509ae0162b',1,'SemanticSearchEvaluation::ApproachComparison']]],
  ['runtest_171',['RunTest',['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a888f606289608fb4bd112d534c085e1a',1,'SemanticSearchEvaluation::ApproachComparison::ElasticsearchEvaluation']]]
];

var searchData=
[
  ['foaf_204',['FOAF',['../class_semantic_search_evaluation_1_1_uris.html#aa8123a783b21fefc2c1170add49a1dad',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5ffamily_5fname_205',['FOAF_FAMILY_NAME',['../class_semantic_search_evaluation_1_1_uris.html#a728d49e26669d03f7b5d6754fa79fc2c',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fgiven_5fname_206',['FOAF_GIVEN_NAME',['../class_semantic_search_evaluation_1_1_uris.html#abb6f345f9d107d524fb370741f411144',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fname_207',['FOAF_NAME',['../class_semantic_search_evaluation_1_1_uris.html#a041341121f89212a5071379840f7f9b7',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fperson_208',['FOAF_PERSON',['../class_semantic_search_evaluation_1_1_uris.html#a7e694434311ba2f54c3d2e4b55dad9d1',1,'SemanticSearchEvaluation::Uris']]]
];

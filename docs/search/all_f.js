var searchData=
[
  ['rdf_79',['RDF',['../class_semantic_search_evaluation_1_1_uris.html#a262c2471fd3bb650cd2365fd8cfce766',1,'SemanticSearchEvaluation::Uris']]],
  ['rdf_5ftype_5flong_80',['RDF_TYPE_LONG',['../class_semantic_search_evaluation_1_1_uris.html#a71bbde5cf3d71a4917270bcf22111db2',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_81',['RDFS',['../class_semantic_search_evaluation_1_1_uris.html#a42a6c7390740314a11740d98d98dc110',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_5flabel_82',['RDFS_LABEL',['../class_semantic_search_evaluation_1_1_uris.html#a1e272be7de6ed699df2138f710b30c6d',1,'SemanticSearchEvaluation::Uris']]],
  ['rdfs_5fsubclass_5fof_83',['RDFS_SUBCLASS_OF',['../class_semantic_search_evaluation_1_1_uris.html#af8cfe517d54f5df7912c8b7f0123a158',1,'SemanticSearchEvaluation::Uris']]],
  ['readme_2emd_84',['readme.md',['../readme_8md.html',1,'']]],
  ['regexsearch_85',['RegexSearch',['../class_semantic_search_evaluation_1_1_regex_search.html',1,'SemanticSearchEvaluation']]],
  ['regexsearch_2ecs_86',['RegexSearch.cs',['../_regex_search_8cs.html',1,'']]],
  ['reindex_87',['Reindex',['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a54c6f787933dd0a0bf5676abeb7c2bf2',1,'SemanticSearchEvaluation.ApproachComparison.ElasticsearchEvaluation.Reindex()'],['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a72a0c9518e92b591e33d80433dedc051',1,'SemanticSearchEvaluation.ApproachComparison.ElasticsearchEvaluation.REINDEX()']]],
  ['removegraph_88',['RemoveGraph',['../class_semantic_search_evaluation_1_1_virtuoso_connector.html#a686884bb7084aa981aa60d48bd4344e8',1,'SemanticSearchEvaluation::VirtuosoConnector']]],
  ['runallapproaches_89',['RunAllApproaches',['../class_semantic_search_evaluation_1_1_approach_comparison.html#ae5eadf24b4cf5fa7a8c6ea0196397b0c',1,'SemanticSearchEvaluation::ApproachComparison']]],
  ['runempty_90',['RunEmpty',['../class_semantic_search_evaluation_1_1_approach_comparison.html#a0aad4979e2e91d45b8edf2509ae0162b',1,'SemanticSearchEvaluation::ApproachComparison']]],
  ['runtest_91',['RunTest',['../class_semantic_search_evaluation_1_1_approach_comparison_1_1_elasticsearch_evaluation.html#a888f606289608fb4bd112d534c085e1a',1,'SemanticSearchEvaluation::ApproachComparison::ElasticsearchEvaluation']]]
];

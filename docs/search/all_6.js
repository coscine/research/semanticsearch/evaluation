var searchData=
[
  ['facetedsearch_44',['FacetedSearch',['../class_semantic_search_evaluation_1_1_faceted_search.html',1,'SemanticSearchEvaluation']]],
  ['facetedsearch_2ecs_45',['FacetedSearch.cs',['../_faceted_search_8cs.html',1,'']]],
  ['foaf_46',['FOAF',['../class_semantic_search_evaluation_1_1_uris.html#aa8123a783b21fefc2c1170add49a1dad',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5ffamily_5fname_47',['FOAF_FAMILY_NAME',['../class_semantic_search_evaluation_1_1_uris.html#a728d49e26669d03f7b5d6754fa79fc2c',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fgiven_5fname_48',['FOAF_GIVEN_NAME',['../class_semantic_search_evaluation_1_1_uris.html#abb6f345f9d107d524fb370741f411144',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fname_49',['FOAF_NAME',['../class_semantic_search_evaluation_1_1_uris.html#a041341121f89212a5071379840f7f9b7',1,'SemanticSearchEvaluation::Uris']]],
  ['foaf_5fperson_50',['FOAF_PERSON',['../class_semantic_search_evaluation_1_1_uris.html#a7e694434311ba2f54c3d2e4b55dad9d1',1,'SemanticSearchEvaluation::Uris']]]
];

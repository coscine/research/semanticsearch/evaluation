﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Storage;

namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Contains necessary functions to query and manipulate the knowledge graph in Virtuoso.
    /// </summary>
    public class VirtuosoConnector
    {
        //private readonly List<string> RULESET_GRAPHS = new List<string>()
        //{
        //    "http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/",
        //    "http://www.w3.org/ns/org#",
        //    "http://xmlns.com/foaf/0.1/"
        //};

        private readonly IDictionary<string, Uri> NAMESPACES = new Dictionary<string, Uri>()
        {
            { "dcterms", new Uri(Uris.DCTERMS) },
            { "rdfs", new Uri(Uris.RDFS) },
            { "rdf", new Uri(Uris.RDF) },
            { "sh", new Uri(Uris.SHACL) },
            { "foaf", new Uri(Uris.FOAF) },
            { "coscineprojectstructure", new Uri(Uris.COSCINE_PROJECTSTRUCTURE) },
            { "owl", new Uri(Uris.OWL) },
            { "qudt", new Uri(Uris.QUDT) },
            { "coscinesearch", new Uri(Uris.COSCINE_SEARCH) },
            { "engmeta", new Uri(Uris.ENGMETA) },
            { "unit", new Uri(Uris.UNIT) },
            { "org", new Uri(Uris.ORG) }
        };

        private readonly SparqlParameterizedString _queryString;
        private readonly VirtuosoManager _manager;

        public VirtuosoConnector(string server = "localhost", int dbPort = 1111, string db = "db", string dbUser = "dba", string dbPassword = "dba", string virtuosoISQLLocation = "C:/Programs/Virtuoso/bin/isql.exe")
        {
            _queryString = new SparqlParameterizedString();
            _manager = new VirtuosoManager(server, dbPort, db, dbUser, dbPassword);

            foreach (var nameSpace in NAMESPACES)
            {
                _queryString.Namespaces.AddNamespace(nameSpace.Key, nameSpace.Value);
            }

            // define rulesets for inference, only needs to be done if Virtuoso is restarted
            //foreach (var graph in RULESET_GRAPHS)
            //{
            //    ExecuteCommand(
            //        "powershell.exe",
            //        $"\"\\\"rdfs_rule_set ('ruleset', '{graph}') ;\\\" | {virtuosoISQLLocation}\""
            //    );
            //}
        }

        /// <summary>
        /// Returns a metadata graph.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <returns>A graph.</returns>
        public Graph GetGraph(string graphName)
        {
            var graph = new Graph();
            _manager.LoadGraph(graph, graphName);
            return graph;
        }


        /// <summary>
        /// Saves a metadata graph.
        /// </summary>
        /// <param name="graph">Metadata graph.</param>
        public void SaveGraph(IGraph graph)
        {
            _manager.SaveGraph(graph);
        }

        /// <summary>
        /// Removes a metadata graph.
        /// </summary>
        /// <param name="graph">Metadata graph.</param>
        public void RemoveGraph(IGraph graph)
        {
            _manager.DeleteGraph(graph.BaseUri);
        }

        /// <summary>
        /// Queries the knowledge graph with a query.
        /// </summary>
        /// <param name="query">String representation of a SPARQL query.</param>
        /// <returns>A <c>SparqlResultSet</c>.</returns>
        public SparqlResultSet QueryWithResultSet(string query)
        {
            _queryString.CommandText = query;
            return (SparqlResultSet)_manager.Query(_queryString.ToString());
        }

        /// <summary>
        /// Executes an executable file with the given arguments.
        /// </summary>
        /// <param name="fileName">Path to executable.</param>
        /// <param name="arguments">Arguments for the executable.</param>
        //private static void ExecuteCommand(string fileName, string arguments)
        //{
        //    ProcessStartInfo startInfo = new ProcessStartInfo
        //    {
        //        FileName = fileName,
        //        Arguments = arguments,
        //        RedirectStandardOutput = true,
        //        RedirectStandardError = true,
        //        UseShellExecute = false,
        //        CreateNoWindow = true,
        //    };
        //    using (var process = new Process
        //    {
        //        StartInfo = startInfo
        //    })
        //    {
        //        process.Start();

        //        string output = process.StandardOutput.ReadToEnd();
        //        Console.WriteLine(output);

        //        string errors = process.StandardError.ReadToEnd();
        //        Console.WriteLine(errors);
        //    }
        //}
    }
}
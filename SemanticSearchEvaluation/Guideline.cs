﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Guideline for search results.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class Guideline : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
            $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms:  <http://purl.org/dc/terms/>
                PREFIX org:  <http://www.w3.org/ns/org#>
                SELECT ?s WHERE {{
                    ?s a ?profile.
                    ?profile a sh:NodeShape.
                    {{
                        SELECT ?s {{
                            ?s dcterms:publisher <https://www.rwth-aachen.de/22000> .
                        }}
                    }}
                    UNION
                    {{
                        SELECT ?s {{ 
                            ?s dcterms:publisher ?person .
                            ?membership a org:Membership. 
                            ?membership org:member ?person.
                            ?membership org:organization <https://www.rwth-aachen.de/22000>
                        }}
                    }}
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                SELECT ?s WHERE {{
                    ?s engmeta:version 10 .
                    ?s a ?profile.
                    ?profile a sh:NodeShape.                        
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms:  <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:created ?date .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                    FILTER (?date >= xsd:date(\""2007-01-01\"") && ?date < xsd:date(\""2008-01-01\""))
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms:  <http://purl.org/dc/terms/>
                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                SELECT DISTINCT ?s WHERE {{
                    {{
                        SELECT ?s {{
                            ?s dcterms:abstract ?abstract .
                            ?abstract bif:contains \""'design patterns'\"" .
                            FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                        }}
                    }}
                    UNION
                    {{
                        SELECT ?s {{
                            ?s dcterms:title ?title .
                            ?title bif:contains \""'design patterns'\"" .
                            FILTER (lang(?title) = 'en' || lang(?title) = '') .
                        }}
                    }}
                    ?s engmeta:version ?version .
                    FILTER (?version < 5).
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 

                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms:  <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    {{
                        SELECT ?s {{
                            ?s dcterms:abstract ?abstract .
                            ?abstract bif:contains \""'computer science'\"" .
                            FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                        }}                    
                    }}
                    UNION 
                    {{
                        SELECT ?s {{
                            ?s dcterms:subject ?subject .                        
                            ?class rdfs:subClassOf* ?superclass .
                            ?subject a ?class .   
                            FILTER (STR(?superclass) = \""http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=409\"")                                
                        }}
                    }}

                ?s a ?profile.
                ?profile a sh:NodeShape. 
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                PREFIX coscineprojectstructure: <https://purl.org/coscine/terms/projectstructure#>
                SELECT ?s WHERE {{
                    ?s engmeta:version ?version .
                    ?s coscineprojectstructure:isFileOf <https://purl.org/coscine/vocabularies/resource#resource2> .
                    {{
                      SELECT ?newestVersion
                          WHERE {{
                                ?file engmeta:version ?newestVersion .
                                ?file coscineprojectstructure:isFileOf <https://purl.org/coscine/vocabularies/resource#resource2> .
                          }} ORDER BY DESC(?newestVersion) LIMIT 1
                    }} .
                    FILTER( ?version = ?newestVersion) .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                SELECT ?s WHERE {{
                    ?s engmeta:mode ?mode .
                    FILTER ((?mode = <https://purl.org/coscine/terms/mode#experiment> || ?mode = <https://purl.org/coscine/terms/mode#simulation>) && ?mode != <https://purl.org/coscine/terms/mode#analysis> )
                    ?s a ?profile.
                    ?profile a sh:NodeShape.
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:abstract ?abstract .
                    ?abstract bif:contains \""'human consciousness' AND non-algorithmic\"" .
                    FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:available \""2020-07-03+02:00\""^^xsd:date.
                    ?s a ?profile.
                    ?profile a sh:NodeShape.
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:abstract ?abstract .
                    ?abstract bif:contains \""left\"" .
                    FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                SELECT ?s WHERE {{
                    {{
                        ?s engmeta:controlledVariable <https://purl.org/coscine/terms/variable#variable1>.
                    }}
                    UNION
                    {{
                        ?s engmeta:measuredVariable <https://purl.org/coscine/terms/variable#variable1>.
                    }}
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms:  <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:issued ?date .
                    ?s dcterms:abstract ?abstract .
                    ?abstract bif:contains \""'object-oriented software'\"" .
                    FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                    FILTER (?date < xsd:date(\""2015-01-01\""))
                }}
           ",
           $@"
                PREFIX sh: <http://www.w3.org/ns/shacl#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                SELECT ?s WHERE {{
                    ?s dcterms:publisher <http://dbpedia.org/resource/Thomas_Pynchon> .
                    ?s dcterms:created ?date .
                    ?s dcterms:abstract ?abstract .
                    ?abstract bif:contains \""'object-oriented software'\"" .
                    FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                    FILTER (?date >= xsd:date(\""2016-01-01\"") && ?date < xsd:date(\""2017-01-01\"")) .
                    ?s a ?profile.
                    ?profile a sh:NodeShape. 
                }}
           "
        };

        public override string GetName()
        {
            return "guideline";
        }

        public override string GetPath()
        {
            return "./../../../Query/bin/Debug/Query.exe";
        }

        public override string GetArguments(int counter)
        {
            return $@"""{_queries[counter].Replace("\r\n", "")}""";
        }
    }
}
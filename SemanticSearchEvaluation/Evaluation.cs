﻿using CommandLine;
using GenerateMetadataRecords;
using System;

namespace SemanticSearchEvaluation
{

    /// <summary>
    /// Entry class which interprets the command line arguments and calls corresponding functions.
    /// </summary>
    public class Evaluation
    {
        // provided methods
        private const string SMALL_COMPARISON = "small";
        private const string LARGE_COMPARISON = "large";
        private const string EMPTY = "empty";
        private const string ELASTICSEARCH = "elasticsearch";
        private const string CREATE_METADATA_RECORD = "create";

        /// <summary>
        /// Class to define valid options, and to receive the parsed options.
        /// </summary>
        public class Options
        {
            // required parameter
            [Option('a', "action", Required = true, HelpText = "Possible action: small, large, empty, elasticsearch, create")]
            public string Action { get; set; }

            // dependent on elasticsearch action

            // add/update/delete/reindex
            [Option('t', "test", HelpText = "Possible tests for elasticsearch action: add, update, delete or reindex")]
            public string Test { get; set; }

            // path to metadata records
            [Option('p', "path", HelpText = "Specify path to metadata records for elasticsearch action")]
            public string Path { get; set; }

            // dependent on create 

            // number of metadata records
            [Option('n', "number", HelpText = "Specify the number of metadata records to be generated for create action")]
            public int Number { get; set; }
        }

        public static void Main(string[] args)
        {
            try
            {
                Parser.Default.ParseArguments<Options>(args)
                    .WithParsed<Options>(o =>
                    {
                        switch (o.Action)
                        {
                            case SMALL_COMPARISON:
                                ApproachComparison.RunAllApproaches();
                                break;
                            case LARGE_COMPARISON:
                                ApproachComparison.RunAllApproaches(false);
                                break;
                            case EMPTY:
                                ApproachComparison.RunEmpty();
                                break;
                            case ELASTICSEARCH:
                                if (o.Test == ApproachComparison.ElasticsearchEvaluation.REINDEX)
                                {
                                    ApproachComparison.ElasticsearchEvaluation.Reindex();
                                }
                                else if ((o.Test == ApproachComparison.ElasticsearchEvaluation.ADD
                                      || o.Test == ApproachComparison.ElasticsearchEvaluation.UPDATE
                                      || o.Test == ApproachComparison.ElasticsearchEvaluation.DELETE)
                                      && !String.IsNullOrEmpty(o.Path))
                                {
                                    ApproachComparison.ElasticsearchEvaluation.RunTest(o.Test, o.Path);
                                }
                                else
                                {
                                    Console.WriteLine($"Test parameter does not exist");
                                }
                                break;
                            case CREATE_METADATA_RECORD:
                                if(o.Number != 0)
                                {
                                    GenerateMetadataRecord.Generate(o.Number);
                                } 
                                else
                                {
                                    Console.WriteLine($"Number parameter does not exist");
                                }
                                break;
                            default:
                                Console.WriteLine($"Action parameter does not exist");
                                break;
                        }
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            // lieber einzeln ansätze weil sonst zeiten anders und verbrindung und bricht ab?
            // soll wirkloich schelife oder einzeln... weil ich hab auch einzeln gemacht...: testen auf small was rauskomm...
            // eventuell doof weil bei groß dann abkackt...? wenn ich das mit docker machen ürde wie geht das? also einfach ein run oder so und alles aufgesetzt..?
               // virtuoso db mit 900 die nicht in den 100... 
               // vll auskommentiwrten code für sachen die bräuchte zB vorbereiten
               // restore es....
        }
    }
}
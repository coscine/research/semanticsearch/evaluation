﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Empty search approach.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class EmptySearch : SearchApproach
    {
        public override string GetArguments(int counter)
        {
            return $"\"testQuery\"";
        }

        public override string GetName()
        {
            return "empty";
        }

        public override string GetPath()
        {
            return "./../../../EmptyProject/bin/Debug/EmptyProject.exe";
        }
    }
}
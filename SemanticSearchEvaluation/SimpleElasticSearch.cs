﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Simple Elasticsearch approach.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class SimpleElasticSearch : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
            $@"\""IT Center\""",
            $@"10",
            $@"2007",
            $@"\""design patterns\""",
            $@"\""computer science\""",
            $@"+\""newest version true\"" +\""resource 2\""",
            $@"experiment simulation +-analysis",
            $@"+\""human consciousness\"" +\""non-algorithmic\""",
            $@"\""2020-07-03\""",
            $@"left",
            $@"\""2 meter\""",
            $@"\""object-oriented software\""",
            $@"+\""Thomas Pynchon\"" +2016 +\""object-oriented software\"""
        };

        public override string GetName()
        {
            return "simpleES";
        }

        public override string GetPath()
        {
            return "./../../../SemanticSearchImplementation/SemanticSearchImplementation.exe";
        }

        public override string GetArguments(int counter)
        {
            return $"-a \"search\" -q " + $@"""{_queries[counter]}""";
        }
    }
}

﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Regex approach.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class RegexSearch : SearchApproach
    { 
        private readonly string[] _queries = new string[]
        {
           "IT Center",
           "^10$",
           "2007",
           "(design patterns)|^[0-4]$",
           "computer science",
           "resource 2",
           "experiment|simulation",
           "(human consciousness)|non-algorithmic",
           "2020-07-03",
           "left",
           "2 meter",
           "object-oriented software",
           "(Thomas Pynchon)|2016|(object-oriented software)"     
        };

        /// <summary>
        /// Returns the specified search intention.
        /// </summary>
        /// <param name="counter">Number of search intention.</param>
        /// <returns>Search intention.</returns>
        public string GetQuery(int counter)
        {
            return _queries[counter];
        }

        public override string GetName()
        {
            return "regex";
        }

        public override string GetPath()
        {
            return "./../../../Regex/bin/Debug/Regex.exe";
        }

        public override string GetArguments(int counter)
        {
            return $"\"{_queries[counter]}\"";
        }
    }
}
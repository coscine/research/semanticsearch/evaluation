﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Advanced Elasticsearch approach.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class AdvancedElasticSearch : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
             $@"publisher:\""IT Center\""",
             $@"version:10",
             $@"date_created:[2007-01-01 TO 2007-12-31]",
             $@"\""design patterns\"" AND version:<5",
             $@"\""computer science\""",
             $@"is_file_of:\""resource 2\"" AND is_newest_version:true",
             $@"mode:((experiment OR simulation) AND NOT analysis)",
             $@"abstract:(\""human consciousness\"" AND non-algorithmic)",
             $@"date_available:2020-07-03",
             $@"left",
             $@"controlled_variables:\""2 meter\"" measured_variables:\""2 meter\""",
             $@"abstract:\""object-oriented software\"" AND date_issued:{{* TO 2015-01-01}}",
             $@"publisher:\""Thomas Pynchon\"" AND date_created_year:2016 AND abstract:\""object-oriented software\"""         
        };

        public override string GetArguments(int counter)
        {
            return $"-a \"search\" --adv true -q " + $@"""{_queries[counter]}""";
        }

        public override string GetName()
        {
            return "advancedES";
        }

        public override string GetPath()
        {
            return "./../../../SemanticSearchImplementation/SemanticSearchImplementation.exe";
        }
    }
}
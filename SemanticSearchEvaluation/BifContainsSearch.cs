﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Bif:contains approach.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class BifContainsSearch : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
            "'IT Center'",
           "'10'",
           "'2007'",
           "'design patterns'",
           "'computer science'",
           "'resource 2'",
           "(experiment OR simulation) AND NOT analysis",
           "'human consciousness' AND 'non-algorithmic'",
           "'20-07-03'",
           "left",
           "'2 meter'",
           "'object-oriented software'",
           "'Thomas Pynchon' OR '2016' OR 'object-oriented software'"
        };

        public override string GetPath()
        {
            return "./../../../BifContains/bin/Debug/BifContains.exe";
        }

        public override string GetArguments(int counter)
        {
            return $"\"{_queries[counter]}\"";
        }

        public override string GetName()
        {
            return "bifcontains";
        }
    }
}
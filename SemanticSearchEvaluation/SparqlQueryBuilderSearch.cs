﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// SPARQL Query builder.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class SparqlQueryBuilderSearch : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
                                              $@"
                                   PREFIX sh: <http://www.w3.org/ns/shacl#>
                                   PREFIX dcterms:  <http://purl.org/dc/terms/>
                                   PREFIX org:  <http://www.w3.org/ns/org#>
                                   SELECT ?s WHERE {{
                                       ?s a ?profile.
                                       ?profile a sh:NodeShape.
                                       {{
                                           SELECT ?s {{
                                               ?s dcterms:publisher <https://www.rwth-aachen.de/22000> .
                                           }}
                                       }}
                                        UNION
                                       {{
                                           SELECT ?s {{
                                               ?s dcterms:publisher ?publisher .
                                                [] a org:Membership ;
            org:member ?publisher;
            org:organization <https://www.rwth-aachen.de/22000> 

                                           }}                    
                                       }}
                                   }}
                             ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                       SELECT ?s WHERE {{
                           ?s engmeta:version 10 .
                           ?s a ?profile.
                           ?profile a sh:NodeShape.                        
                       }}
                  ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX dcterms:  <http://purl.org/dc/terms/>
                       SELECT ?s WHERE {{
                           ?s dcterms:created ?date .
                           ?s a ?profile.
                           ?profile a sh:NodeShape. 
                           FILTER (?date >= xsd:date(\""2007-01-01\"") && ?date < xsd:date(\""2008-01-01\""))
                       }}
                  ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX dcterms:  <http://purl.org/dc/terms/>
                       PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                       SELECT DISTINCT ?s WHERE {{
                           {{
                               SELECT ?s {{
                                   ?s dcterms:abstract ?abstract .
                                   ?abstract bif:contains \""'design patterns'\"" .
                                   FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                               }}
                           }}
                           UNION
                           {{
                               SELECT ?s {{
                                   ?s dcterms:title ?title .
                                   ?title bif:contains \""'design patterns'\"" .
                                   FILTER (lang(?title) = 'en' || lang(?title) = '') .
                               }}
                           }}
                           ?s engmeta:version ?version .
                           FILTER (?version < 5).
                           ?s a ?profile.
                           ?profile a sh:NodeShape. 

                       }}
                  ",
                  $@"
                                                            PREFIX sh: <http://www.w3.org/ns/shacl#>
                                       PREFIX dcterms:  <http://purl.org/dc/terms/>
                                       SELECT DISTINCT ?s WHERE {{
                                           {{
                                               SELECT ?s {{
                                                   ?s dcterms:abstract ?abstract .
                                                   ?abstract bif:contains \""'computer science'\"" .
                                                   FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                                               }}                    
                                           }}
                                           UNION 
                                           {{
                                               SELECT ?s {{
                                                   ?s dcterms:subject ?subject .
            ?subject rdfs:subClassOf* <http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=409>   .                             
                                               }}
                                           }}
                                           ?s a ?profile.
                                           ?profile a sh:NodeShape. 
                                       }}
                                  ",
                                                 $@"
                                PREFIX sh: <http://www.w3.org/ns/shacl#>
                                PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                                PREFIX coscineprojectstructure: <https://purl.org/coscine/terms/projectstructure#>
                                SELECT ?s WHERE {{
                                    ?s engmeta:version 10 .
                                    ?s coscineprojectstructure:isFileOf <https://purl.org/coscine/vocabularies/resource#resource2> .      
                                    ?s engmeta:version ?version .
                                    ?s a ?profile.
                                    ?profile a sh:NodeShape. 
                                }} ORDER BY DESC(?version) LIMIT 1
                           ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                       SELECT DISTINCT ?s WHERE {{
                           {{
                               SELECT ?s {{
                                   ?s engmeta:mode <https://purl.org/coscine/terms/mode#experiment>                              
                               }}
                           }}
                           UNION
                           {{
                               SELECT ?s {{
                                   ?s engmeta:mode <https://purl.org/coscine/terms/mode#simulation>                              
                               }}
                           }}
                           ?s engmeta:mode ?mode .
                           FILTER (?mode != <https://purl.org/coscine/terms/mode#analysis> ) .
                           ?s a ?profile.
                           ?profile a sh:NodeShape.
                       }}
                  ",
               $@"
                 PREFIX sh: <http://www.w3.org/ns/shacl#>
                 PREFIX dcterms: <http://purl.org/dc/terms/>
                 SELECT ?s WHERE {{
                     ?s dcterms:abstract ?abstract .
                     ?abstract bif:contains \""'human consciousness' AND non-algorithmic\"" .
                     FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                     ?s a ?profile.
                     ?profile a sh:NodeShape. 
                 }}
            ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX dcterms: <http://purl.org/dc/terms/>
                       SELECT ?s WHERE {{
                           ?s dcterms:available \""2020-07-03+02:00\""^^xsd:date.
                           ?s a ?profile.
                           ?profile a sh:NodeShape.
                       }}
                  ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX dcterms: <http://purl.org/dc/terms/>
                       SELECT ?s WHERE {{
                           ?s dcterms:abstract ?abstract .
                           ?abstract bif:contains \""left\"" .
                           FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                           ?s a ?profile.
                           ?profile a sh:NodeShape. 
                       }}
                  ",
               $@"
                                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                                       PREFIX engmeta:  <http://www.ub.uni-stuttgart.de/dipling#>
                                       PREFIX qudt:  <http://qudt.org/schema/qudt/>
            prefix unit: <http://qudt.org/vocab/unit/> 
                                       SELECT DISTINCT ?s WHERE {{
                                           {{
                                               SELECT ?s {{
                                                   ?s engmeta:controlledVariable ?var .
            ?var   qudt:unit unit:M .
             ?var qudt:value \""2\"" .
                                               }}
                                           }}
                                           UNION
                                           {{
                                               SELECT ?s {{
                                                   ?s engmeta:measuredVariable ?var .
            ?var   qudt:unit unit:M .
             ?var qudt:value \""2\"" .

                                               }}
                                           }}
                                           ?s a ?profile.
                                           ?profile a sh:NodeShape. 
                                       }}
                                  ",
                                                 $@"
                                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                                       PREFIX dcterms:  <http://purl.org/dc/terms/>
                                       SELECT ?s WHERE {{
                                           ?s dcterms:issued ?date .
                                           ?s dcterms:abstract ?abstract .
                                         ?abstract bif:contains \""'object-oriented software'\"" .
                                           FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                                           ?s a ?profile.
                                           ?profile a sh:NodeShape. 
                                           FILTER (?date < xsd:date(\""2015-01-01\""))
                                       }}
                                  ",
                  $@"
                       PREFIX sh: <http://www.w3.org/ns/shacl#>
                       PREFIX dcterms: <http://purl.org/dc/terms/>
                       SELECT ?s WHERE {{
                           ?s dcterms:publisher <http://dbpedia.org/resource/Thomas_Pynchon> .
                           ?s dcterms:created ?date .
                           ?s dcterms:abstract ?abstract .
                           ?abstract bif:contains \""'object-oriented software'\"" .
                           FILTER (lang(?abstract) = 'en' || lang(?abstract) = '') .
                           FILTER (?date >= xsd:date(\""2016-01-01\"") && ?date < xsd:date(\""2017-01-01\"")) .
                           ?s a ?profile.
                           ?profile a sh:NodeShape. 
                       }}
                  "
        };

        public override string GetName()
        {
            return "builder";
        }

        public override string GetPath()
        {
            return "./../../../Query/bin/Debug/Query.exe";
        }

        public string GetQuery(int i)
        {
            return _queries[i];
        }

        public override string GetArguments(int counter)
        {
            return $@"""{_queries[counter].Replace("\r\n", "")}""";
        }
    }
}
﻿using System.Diagnostics;

namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Provides all necessary functions for the evaluation of a search approach.
    /// </summary>
    public abstract class SearchApproach
    {
        /// <summary>
        /// The process which needs to be executed to run the specified search approach.
        /// </summary>
        /// <param name="path">Path to the executable of the search approach.</param>
        /// <param name="arguments">Arguments for the process.</param>
        /// <returns>Process to execute specific search intention of specific approach.</returns>
        public Process GetProcess(string path, string arguments)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = path,
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            return process;
        }

        /// <summary>
        /// The path to the executable of the search approach. 
        /// </summary>
        /// <returns>String with the relative path.</returns>
        public abstract string GetPath();

        /// <summary>
        /// The arguments for the process containing the specified search intention.
        /// </summary>
        /// <param name="counter">Number of the search intention.</param>
        /// <returns></returns>
        public abstract string GetArguments(int counter);

        /// <summary>
        /// The specified name for the search approach.
        /// </summary>
        /// <returns>Name of the search approach.</returns>
        public abstract string GetName();
    }
}
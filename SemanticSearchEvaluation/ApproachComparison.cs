﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using VDS.RDF;

namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Contains all functions for the evaluation of the search approaches and further elasticsearch tests.
    /// </summary>
    public class ApproachComparison
    {
        // abbreviated textual representation of search intentions
        private static readonly string[] intentions = new string[] {
            "Published at IT Center",
            "Version 10",
            "Created in 2007",
            "Design Patterns and Version < 5",
            "Computer Science",
            "Newest Version Res. 2",
            "Experi./Simu. not Anal.",
            "Non Comput. Human Consci.",
            "Av. s. 03.07.20",
            "Left (political)",
            "2 Meters",
            "Pub. < 2015 Oo. Softw.",
            "Pub. T. Pyn. Created 2016 Oo Softw."
        };

        private static readonly VirtuosoConnector connector = new VirtuosoConnector();

        /// <summary>
        /// Runs the tests for all evaluated search approaches.
        /// </summary>
        /// <param name="small">Specifies if the small or large data record is used.</param>
        public static void RunAllApproaches(bool small = true)
        {
            // all evaluated search approaches
            List<SearchApproach> approaches = new List<SearchApproach>() {
                new Guideline(),
                new FacetedSearch(),
                new RegexSearch(),
                new AdvancedElasticSearch(),
                new SimpleElasticSearch(),
                new BifContainsSearch(),
                new SparqlQueryBuilderSearch()
            };

            foreach (var approach in approaches)
            {
                var size = small ? "small" : "large";
                // only print result of small data record
                if (small)
                {
                    PrintResults(approach, size);
                }
                MeasureTime(approach, size);
            }
        }
            
        public static void RunEmpty()
        {
            MeasureTime(new EmptySearch(), "empty");
        }

        /// <summary>
        /// Runs and prints the calculated times for all search intentions.
        /// </summary>
        /// <param name="approach">Search approach which is evaluated.</param>
        /// <param name="name">Specifies if the small or large data record is used.</param>
        private static void MeasureTime(SearchApproach approach, string name = "small")
        {
            Stopwatch stopWatch = new Stopwatch();
            for (var i = 0; i < intentions.Length; i++)
            {
                using (StreamWriter file = File.CreateText($@"./../../../evaluation_results/time/{name}/{approach.GetName()}/time_{i + 1}.txt"))
                {
                    file.WriteLine("search intention: " + intentions[i] + System.Environment.NewLine);

                    var times = new List<long>();

                    for (var j = 0; j < 101; j++)
                    {
                        var process = approach.GetProcess(approach.GetPath(), approach.GetArguments(i));
                        stopWatch.Reset();
                        stopWatch.Start();
                        process.Start();
                        process.WaitForExit();
                        stopWatch.Stop();
                        // prints all measures times, including the first one which is not taken into account when calculating average time, max and standard deviation
                        file.WriteLine(stopWatch.ElapsedMilliseconds);
                        if (j != 0)
                        {
                            // jump over first time
                            times.Add(stopWatch.ElapsedMilliseconds);
                        }
                        Thread.Sleep(100);
                    }

                    CalcValues(file, times);

                    times.Clear();
                }
            }
        }

        /// <summary>
        /// Calcutlates average time, max and standard deviation.
        /// </summary>
        /// <param name="file"><c>StreamWriter</c> where to write results.</param>
        /// <param name="times">List of calculated times.</param>
        private static void CalcValues(StreamWriter file, List<long> times)
        {
            var avg = Math.Round(times.Average(), 2);
            file.WriteLine("avg time: " + avg);

            var maxDeviation = times.Select(x => Math.Abs(x - avg)).Max();
            file.WriteLine("max deviation: " + maxDeviation);

            double sumOfSquaresOfDifferences = times.Select(val => (val - avg) * (val - avg)).Sum();
            double standardDeviation = Math.Sqrt(sumOfSquaresOfDifferences / times.Count);
            file.WriteLine("standard deviation: " + Math.Round(standardDeviation));
        }

        /// <summary>
        /// Runs and prints the results for all search intentions.
        /// </summary>
        /// <param name="approach">Search approach which is evaluated.</param>
        /// <param name="size">Specifies if the small or large data record is used.</param>
        private static void PrintResults(SearchApproach approach, string size = "small")
        {
            for (var i = 0; i < intentions.Length; i++)
            {
                using (StreamWriter file = File.CreateText($@"./../../../evaluation_results/results/{size}/{approach.GetName()}/result_{i + 1}.txt"))
                {
                    file.WriteLine("search intention: " + intentions[i] + System.Environment.NewLine);

                    var process = approach.GetProcess(approach.GetPath(), approach.GetArguments(i));

                    process.Start();

                    // print results
                    while (!process.StandardOutput.EndOfStream)
                    {
                        var line = process.StandardOutput.ReadLine();
                        file.WriteLine(line);
                    }

                    process.WaitForExit();
                }
            }
        }  

        /// <summary>
        /// Functions for Elasticsearch Evaluation.
        /// </summary>
        public class ElasticsearchEvaluation
        {
            // provided tests for elasticsearch
            public const string REINDEX = "reindex";
            public const string ADD = "add";
            public const string UPDATE = "update";
            public const string DELETE = "delete";

            private static readonly SearchApproach approach = new AdvancedElasticSearch();

            /// <summary>
            /// Reindexes all metadata graphs from Virtuoso.
            /// </summary>
            public static void Reindex()
            {
                Stopwatch stopwatch = new Stopwatch();
                var process = approach.GetProcess(approach.GetPath(), $"-a \"reindex\"");
                using (StreamWriter file = File.CreateText($@"./../../../evaluation_results/time/ES/reindexing.txt"))
                {
                    var times = new List<long>();

                    for (var i = 0; i < 10; i++)
                    {
                        stopwatch.Reset();
                        stopwatch.Start();
                        process.Start();
                        process.WaitForExit();
                        stopwatch.Stop();
                        file.WriteLine(stopwatch.ElapsedMilliseconds);
                        times.Add(stopwatch.ElapsedMilliseconds);
                        //Thread.Sleep(10000);
                    }
                    CalcValues(file, times);
                }
            }

            /// <summary>
            /// Runs the specified elasticsearch test.
            /// </summary>
            /// <param name="test">Test which should be executed: add/update/delete.</param>
            /// <param name="path">Path to the metdata record folder from the sample dataset.</param>
            public static void RunTest(string test, string path)
            {
                Stopwatch stopwatch = new Stopwatch();

                var times = new List<long>();

                using (StreamWriter file = File.CreateText($"./../../../evaluation_results/time/ES/{test}.txt"))
                {
                    foreach (var graph in Directory.GetFiles(path + "/es_100"))
                    {
                        IGraph g = new Graph();
                        g.LoadFromFile(graph);
                        if (test == ADD || test == UPDATE)
                        {
                            connector.SaveGraph(g);
                        }                        
                        var process = approach.GetProcess(approach.GetPath(), $"-a \"{test}\" -d " + $@"""{g.BaseUri.ToString()}""");
                        stopwatch.Reset();
                        stopwatch.Start();
                        process.Start();
                        process.WaitForExit();
                        stopwatch.Stop();
                        if (test == DELETE)
                        {
                            connector.RemoveGraph(g);
                        }
                        file.WriteLine(stopwatch.ElapsedMilliseconds);
                        times.Add(stopwatch.ElapsedMilliseconds);
                        //Thread.Sleep(100);
                    }

                    CalcValues(file, times);
                }
            }           
        }
    }
}
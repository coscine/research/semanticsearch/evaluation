﻿namespace SemanticSearchEvaluation
{
    /// <summary>
    /// Faceted search.
    /// </summary>
    /// <inheritdoc cref="SearchApproach"/>
    public class FacetedSearch : SearchApproach
    {
        private readonly string[] _queries = new string[]
        {
            $@"{{?s dcterms:publisher <https://www.rwth-aachen.de/22000> }} UNION {{ ?s dcterms:publisher ?publisher .  ?membership a org:Membership.  ?membership org:member ?publisher . ?membership org:organization <https://www.rwth-aachen.de/22000>}} ",
            $@"?s engmeta:version 10",
            $@"?s dcterms:created ?date . FILTER (?date >= xsd:date(\""2007-01-01\"") && ?date < xsd:date(\""2008-01-01\"") )",
            $@"?s engmeta:version 3",
            $@"",
            $@"?s engmeta:version 10",
            $@"{{?s engmeta:mode <https://purl.org/coscine/terms/mode#experiment>}} UNION {{?s engmeta:mode <https://purl.org/coscine/terms/mode#simulation>}}",
            $@"",
            $@"?s dcterms:available ?date . FILTER (?date >= xsd:date(\""2020-01-01\"") &&?date < xsd:date(\""2021-01-01\""))",
            $@"",
            $@"?s engmeta:controlledVariable ?var . ?var qudt:unit unit:M . ?var qudt:value \""2\"" . ?s engmeta:measuredVariable ?var . ?var qudt:unit unit:M . ?var qudt:value \""2\""",
            $@"",
            $@"?s dcterms:publisher <http://dbpedia.org/resource/Thomas_Pynchon> . ?s dcterms:created ?date . FILTER (?date >= xsd:date(\""2016-01-01\"") && ?date < xsd:date(\""2017-01-01\""))"
        };

        public override string GetName()
        {
            return "faceted";
        }

        public override string GetPath()
        {
            return "./../../../FacetedSearch/bin/Debug/FacetedSearch.exe";
        }

        public override string GetArguments(int counter)
        {
            var regexSearch = new RegexSearch();
            return $"\"{regexSearch.GetQuery(counter)}\" " + $@"""{_queries[counter]}""";
        }
    }
}
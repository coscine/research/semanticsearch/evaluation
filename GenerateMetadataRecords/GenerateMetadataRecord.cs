﻿using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Writing;

namespace GenerateMetadataRecords
{
    /// <summary>
    /// Functions to generate engmeta2 metadata records.
    /// </summary>
    public class GenerateMetadataRecord
    {
        private static readonly VirtuosoManager manager = new VirtuosoManager("localhost", VirtuosoManager.DefaultPort, VirtuosoManager.DefaultDB, "dba", "dba");
        
        // properties of engmeta profile
        const string WORKED = "http://www.ub.uni-stuttgart.de/dipling#worked";
        const string WORKEDNOTE = "http://www.ub.uni-stuttgart.de/dipling#workednote";
        const string VERSION = "http://www.ub.uni-stuttgart.de/dipling#version";
        const string MODE = "http://www.ub.uni-stuttgart.de/dipling#mode";
        const string MEASURED_VARIABLE = "http://www.ub.uni-stuttgart.de/dipling#measuredVariable";
        const string CONTROLLED_VARIABLE = "http://www.ub.uni-stuttgart.de/dipling#controlledVariable";
        const string STEP = "http://www.ub.uni-stuttgart.de/dipling#step";
        const string IS_FILE_OF = "https://purl.org/coscine/terms/projectstructure#isFileOf";
        const string ENGMETA = "https://purl.org/coscine/ap/engmeta2/";
        const string SH_PATH = "http://www.w3.org/ns/shacl#path";
        const string TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
        const string DFG = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/";
        const string SUBJECT = "http://purl.org/dc/terms/subject";
        const string CREATOR = "http://purl.org/dc/terms/creator";
        const string ABSTRACT = "http://purl.org/dc/terms/abstract";
        const string INVESTIGATION_KEYWORD = "http://www.purl.org/net/CSMD/4.0#investigation_keyword";
        const string TITLE = "http://purl.org/dc/terms/title";
        const string FORMAT = "http://purl.org/dc/terms/format";
        const string LICENSE = "http://purl.org/dc/terms/license";
        const string PUBLISHER = "http://purl.org/dc/terms/publisher";
        const string CREATED = "http://purl.org/dc/terms/created";
        const string ISSUED = "http://purl.org/dc/terms/issued";
        const string AVAILABLE = "http://purl.org/dc/terms/available";
        const string DCMI_TYPE = "http://purl.org/dc/terms/type";
        const string RIGHTS = "http://purl.org/dc/terms/rights";
        const string XSD_BOOLEAN = "http://www.w3.org/2001/XMLSchema#boolean";
        const string XSD_INTEGER = "http://www.w3.org/2001/XMLSchema#integer";

        // example data
        private static readonly string[] SUBJECTS = CreateSubjects();
        private static readonly string[] TYPES = CreateTypes();
        private static readonly string[] LICENSES = CreateLicenses();
        private static readonly string[] FORMATS = CreateFormats();
        private static readonly string[] VARIABLES = CreateVariables();
        private static readonly string[] RESOURCES = CreateResources();
        private static readonly string[] STEPS = CreateSteps();
        private static readonly Uri[] PERSON_AND_ORGANIZATION = CreatePersonAndOrganization();

        private static readonly Random random = new Random(22);

        /// <summary>
        /// Generates metdata records.
        /// </summary>
        /// <param name="amount">Number of metadata records to be generated.</param>
        public static void Generate(int amount)
        {
            IGraph shapesGraph = new Graph();
            manager.LoadGraph(shapesGraph, ENGMETA);

            for (int i = 0; i < amount; i++)
            {
                var id = Guid.NewGuid().ToString("N");
                var subject = new Uri("https://purl.org/coscine/md/" + id + "/");
                var dataGraph = new Graph();
                dataGraph.BaseUri = subject;

                dataGraph.NamespaceMap.AddNamespace("dcterms", new Uri("http://purl.org/dc/terms"));
                dataGraph.NamespaceMap.AddNamespace("engmeta", new Uri("http://www.ub.uni-stuttgart.de/dipling#"));
                dataGraph.NamespaceMap.AddNamespace("prov", new Uri("http://www.w3.org/ns/prov#"));
                dataGraph.NamespaceMap.AddNamespace("premis3", new Uri("http://www.loc.gov/premis/rdf/v3/"));
                dataGraph.NamespaceMap.AddNamespace("dctype", new Uri("http://purl.org/dc/dcmitype/"));
                dataGraph.NamespaceMap.AddNamespace("mode", new Uri("https://purl.org/coscine/terms/mode#"));
                dataGraph.NamespaceMap.AddNamespace("license", new Uri("http://spdx.org/licenses/"));
                dataGraph.NamespaceMap.AddNamespace("format", new Uri("https://www.iana.org/assignments/media-types/application/"));

                var subjectNode = dataGraph.CreateUriNode(subject);

                // topic and keywords are dependent on the subject
                string topic = String.Empty;
                bool setTitle = false;
                bool setKeywords = false;

                var person = GetPersonOrOrganization();

                foreach (var triple in shapesGraph.GetTriplesWithPredicate(new Uri(SH_PATH)))
                {
                    var property = triple.Object;
                    if (!(property.NodeType is NodeType.Blank))
                    {
                        var propertyString = property.ToString();
                        INode propertyNode = Tools.CopyNode(property, dataGraph);

                        INode objNode;

                        switch (propertyString)
                        {
                            case PUBLISHER:
                                // sometimes use creator and sometimes an other person
                                var publisher = person;
                                if (random.Next(0, 2) == 1)
                                {
                                    publisher = GetPersonOrOrganization();
                                }
                                objNode = dataGraph.CreateUriNode(publisher);
                                break;
                            case WORKED:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateLiteralNode(GetBoolean(), new Uri(XSD_BOOLEAN));
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case WORKEDNOTE:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateLiteralNode(GetWorkednote());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case SUBJECT:
                                objNode = dataGraph.CreateUriNode(GetSubject());
                                var results = (SparqlResultSet) manager.Query("SELECT ?stripped_label WHERE { <" + objNode.ToString() + "> rdfs:label ?o . BIND(STR(?o) AS ?stripped_label) . FILTER langmatches(lang(?o), 'en')}");
                                topic = results.First().Value("stripped_label").ToString();
                                break;
                            case CREATOR:
                                objNode = dataGraph.CreateUriNode(person);
                                break;
                            case TITLE:
                                if (topic != String.Empty)
                                {
                                    var titleAbstractPair = GetTitle(topic);
                                    objNode = dataGraph.CreateLiteralNode(titleAbstractPair.Key);
                                    dataGraph.Assert(new Triple(subjectNode, dataGraph.CreateUriNode(new Uri(ABSTRACT)), dataGraph.CreateLiteralNode(titleAbstractPair.Value)));

                                }
                                else
                                {
                                    setTitle = true;
                                    continue;
                                }
                                break;
                            case INVESTIGATION_KEYWORD:
                                if (topic != String.Empty)
                                {
                                    // field is optional
                                    if (random.Next(0, 10) == 1)
                                    {
                                        objNode = dataGraph.CreateLiteralNode(GetKeyword(topic));
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    setKeywords = true;
                                    continue;
                                }
                                break;
                            case RIGHTS:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateLiteralNode(GetRight());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case CREATED:
                                objNode = GetDate().ToLiteralDate(dataGraph);
                                break;
                            case ISSUED:
                                // TODO: has to be greater than created
                                var issued = GetDate();
                                if (random.Next(0, 3) == 1)
                                {
                                    issued = DateTime.Now;
                                }
                                objNode = issued.ToLiteralDate(dataGraph);
                                break;
                            case AVAILABLE:
                                var available = GetFutureDate();
                                if (random.Next(0, 3) == 1)
                                {
                                    available = DateTime.Now;
                                }
                                objNode = available.ToLiteralDate(dataGraph);
                                break;
                            case DCMI_TYPE:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateUriNode(GetDCMIType());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case TYPE:
                                objNode = dataGraph.CreateUriNode(new Uri(ENGMETA));
                                break;
                            case VERSION:
                                objNode = dataGraph.CreateLiteralNode(GetVersion(), new Uri(XSD_INTEGER));
                                break;
                            case MODE:
                                objNode = dataGraph.CreateUriNode(GetMode());
                                break;
                            case MEASURED_VARIABLE:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateUriNode(GetVariable());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case CONTROLLED_VARIABLE:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateUriNode(GetVariable());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case IS_FILE_OF:
                                objNode = dataGraph.CreateUriNode(GetResource());
                                break;      
                            case STEP:
                                objNode = dataGraph.CreateUriNode(GetStep());
                                break;
                            case LICENSE:
                                //field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateUriNode(GetLicense());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            case FORMAT:
                                // field is optional
                                if (random.Next(0, 10) == 1)
                                {
                                    objNode = dataGraph.CreateUriNode(GetFormat());
                                }
                                else
                                {
                                    continue;
                                }
                                break;
                            default:
                                continue;
                        }

                        var newTriple = new Triple(subjectNode, propertyNode, objNode);
                        dataGraph.Assert(newTriple);
                    }

                }

                if (setTitle)
                {
                    var titleAbstractPair = GetTitle(topic);
                    var objNode = dataGraph.CreateLiteralNode(titleAbstractPair.Key);
                    dataGraph.Assert(new Triple(subjectNode, dataGraph.CreateUriNode(new Uri(ABSTRACT)), dataGraph.CreateLiteralNode(titleAbstractPair.Value)));
                    var newTriple = new Triple(subjectNode, dataGraph.CreateUriNode(new Uri(TITLE)), objNode);
                    dataGraph.Assert(newTriple);
                }

                if (setKeywords)
                {
                    if (random.Next(0, 10) == 1)
                    {
                        var objNode = dataGraph.CreateLiteralNode(GetKeyword(topic));
                        var newTriple = new Triple(subjectNode, dataGraph.CreateUriNode(new Uri(INVESTIGATION_KEYWORD)), objNode);
                        dataGraph.Assert(newTriple);
                    }
                }

                CompressingTurtleWriter writer = new CompressingTurtleWriter();
                writer.Save(dataGraph, "./../../../data/engmeta2/" + id + ".ttl");

            }
        }

        /// <summary>
        /// Removes special characters in the topic to use it in a SPARQL query.
        /// </summary>
        /// <param name="topic">String containing the topic.</param>
        /// <returns>Topic without special characters.</returns>
        private static string[] PrepareTopic(string topic)
        {
            return topic.Replace("and", String.Empty).Replace("Near", String.Empty).Replace(",", String.Empty).Replace("-", String.Empty).Replace("|", String.Empty).Replace("/", String.Empty).Split(' ').Select(x => x.Trim()).ToArray();
        }

        /** functions to select specific value **/

        private static string GetKeyword(string topic)
        {
            var topics = PrepareTopic(topic);
            if (topics.Count() > 0)
            {
                return topics[0];
            }
            else
            {
                return String.Empty;
            }
        }

        private static Uri GetMode()
        {
            Uri[] modes = new Uri[] { new Uri("https://purl.org/coscine/terms/mode#simulation"), new Uri("https://purl.org/coscine/terms/mode#experiment"), new Uri("https://purl.org/coscine/terms/mode#analysis") };
            var index = random.Next(0, modes.Length);
            return modes[index];
        }

        private static string GetVersion()
        {
            return random.Next(0, 10).ToString();
        }

        private static string GetWorkednote()
        {
            string[] reasons = { "reason1", "reason2", "reason3" };
            var index = random.Next(0, reasons.Length);
            return reasons[index];
        }

        private static string GetBoolean()
        {
            string[] boolean = { "true", "false" };
            var index = random.Next(0, boolean.Length);
            return boolean[index];
        }

        private static DateTime GetFutureDate()
        {
            int range = 2 * 365; // 2 years          
            return DateTime.Today.AddDays(+random.Next(range));
        }

        private static DateTime GetDate()
        {
            int range = 15 * 365; // 15 years          
            return DateTime.Today.AddDays(-random.Next(range));
        }

        private static Uri GetLicense()
        {
            var index = random.Next(0, LICENSES.Length);
            return new Uri(LICENSES[index]);
        }

        private static Uri GetDCMIType()
        {
            var index = random.Next(0, TYPES.Length);
            return new Uri(TYPES[index]);
        }

        private static Uri GetSubject()
        {
            var index = random.Next(0, SUBJECTS.Length);
            return new Uri(SUBJECTS[index]);
        }

        private static Uri GetFormat()
        {
            var index = random.Next(0, FORMATS.Length);
            return new Uri(FORMATS[index]);
        }

        private static Uri GetVariable()
        {
            var index = random.Next(0, VARIABLES.Length);
            return new Uri(VARIABLES[index]);
        }

        private static Uri GetStep()
        {
            var index = random.Next(0, STEPS.Length);
            return new Uri(STEPS[index]);
        }

        private static Uri GetResource()
        {
            var index = random.Next(0, RESOURCES.Length);
            return new Uri(RESOURCES[index]);
        }

        private static Uri GetPersonOrOrganization()
        {
            var index = random.Next(0, PERSON_AND_ORGANIZATION.Length);
            return PERSON_AND_ORGANIZATION[index];
        }


        private static string GetRight()
        {
            string[] rights = { "right1", "right2", "right3" };
            var index = random.Next(0, rights.Length);
            return rights[index];
        }

        public static KeyValuePair<string, string> GetTitle(string topic)
        {
            var topics = topic.Replace("and", String.Empty).Replace("Near", String.Empty).Replace(",", String.Empty).Replace("-", String.Empty).Replace("|", String.Empty).Replace("/", String.Empty).Split(' ').Select(x => x.Trim()).Where(x => x != String.Empty).ToArray();
            topic = string.Join(" OR ", topics);

            // get data from dbpedia
            SparqlRemoteEndpoint dbpedia = new SparqlRemoteEndpoint(new Uri(string.Format("https://dbpedia.org/sparql")));
            SparqlResultSet results = dbpedia.QueryWithResultSet("SELECT DISTINCT ?stripped_label, STR(?abstract) AS ?abstract where {?s dbo:abstract ?abstract . ?s a owl:Thing, bibo:Book, wikidata:Q386724, wikidata:Q571,dbo:Book,dbo:Work,dbo:WrittenWork,schema:Book,schema:CreativeWork .?abstract bif:contains  '(" + topic + ")' .?s rdfs:label ?label . BIND (STR(?label)  AS ?stripped_label) . FILTER langMatches(lang(?label), 'en')}");

            if (results.IsEmpty)
            {
                // TODO: generate random string
                return new KeyValuePair<string, string>();
            }
            else
            {
                var index = random.Next(0, results.Count);
                return new KeyValuePair<string, string>(results.ElementAt(index).Value("stripped_label").ToString(), results.ElementAt(index).Value("abstract").ToString());
            }
        }

        /** functions to prepare possible values **/

        private static string[] CreateSubjects()
        {
            IGraph graph = new Graph();
            manager.LoadGraph(graph, DFG);

            SparqlParameterizedString queryString = new SparqlParameterizedString();
            queryString.Namespaces.AddNamespace("rdfs", new Uri("http://www.w3.org/2000/01/rdf-schema#"));
            queryString.CommandText = "SELECT ?s FROM @dfg {  ?s a ?class . ?class rdfs:subClassOf* @dfg}";
            queryString.SetUri("dfg", new Uri(DFG));
            SparqlQueryParser parser = new SparqlQueryParser();
            SparqlQuery query = parser.ParseFromString(queryString);
            SparqlResultSet results = (SparqlResultSet) graph.ExecuteQuery(query);
            return results.Select(x => x.Value("s").ToString()).ToArray();
        }

        private static string[] CreateFormats()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("SELECT ?s WHERE {?s a <https://www.iana.org/assignments/media-types/> }");
            return results.Select(x => x.Value("s").ToString()).ToArray();

        }

        private static string[] CreateLicenses()
        {
            SparqlResultSet results = (SparqlResultSet)manager.Query("SELECT ?s WHERE {?s a <http://spdx.org/rdf/terms#License>}");
            return results.Select(x => x.Value("s").ToString()).ToArray();

        }

        private static string[] CreateTypes()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("SELECT ?s WHERE {?s a <http://purl.org/dc/dcmitype/> }");
            return results.Select(x => x.Value("s").ToString()).ToArray();

        }

        private static string[] CreateVariables()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("SELECT ?s WHERE {?s a <https://purl.org/coscine/terms/variable/> }");
            return results.Select(x => x.Value("s").ToString()).ToArray();

        }

        private static string[] CreateSteps()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("SELECT ?s WHERE {?s a <https://purl.org/coscine/terms/step/> }");
            return results.Select(x => x.Value("s").ToString()).ToArray();

        }

        private static string[] CreateResources()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("SELECT ?s WHERE {?s a <https://purl.org/coscine/vocabularies/resource/> }");
            return results.Select(x => x.Value("s").ToString()).ToArray();
        }

        private static Uri[] CreatePersonAndOrganization()
        {
            SparqlResultSet results = (SparqlResultSet) manager.Query("DEFINE input:inference 'ruleset' SELECT ?s WHERE {?s a <http://xmlns.com/foaf/0.1/Agent> }");
            return results.Select(x => new Uri(x.Value("s").ToString())).ToArray();
        }
    }
}
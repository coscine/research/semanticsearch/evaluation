This repository contains the code for the evaluation of different approaches for building a search engine for RDF-based metadata records presented in the master thesis ["An Efficient Semantic Search Engine for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09883) as well as the used code to generate example metadata records of engmeta2 profile.

## Evaluation results:

All calculated evaluation results used and presented in the [master thesis](http://dx.doi.org/10.18154/RWTH-2020-09883) can be found in the folder *evaluation_results*.

## Code documentation:

The code documentation can be found under *./docs/index.html*.

## Setup/Prerequisites:

- Virtuoso
    - Virtuoso.ini file 
        - NumberOfBuffers >= 660000
        - MaxDirtyBuffers >= 495000
    - Default settings:
        - Sever: localhost
        - Port: 1111
        - Database: db
        - Database user: dba
        - Database password: dba
    - Define the following graphs as rulesets named "ruleset" for inference rules with `rdfs_rule_set ('<rulesetname>', '<graphname>') ;`
        - http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/
        - http://www.w3.org/ns/org#
        - http://xmlns.com/foaf/0.1/
- Data mentioned in ["Sample Dataset for Search Engine Evaluation for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09886)
    - All named_graphs, use *virtuoso.db/graphs.db*
- Elasticsearch
    - Default settings:
        - Server: localhost
        - Port: 9200
- Specified packages

## How to use:

1. Copy all files from *Release*/*Debug* folder from [SemanticSearchImplementation project](http://dx.doi.org/10.18154/RWTH-2020-09884) (after building it) to *SemanticSearchImplementation* folder
2. (Re-)Build solution (in Debug mode, install/restore necessary packages)
3. Change required data as described below (dependent on test)
4. Start Virtuoso
5. Start Elasticsearch
6. Run *SemanticEvaluation.exe* with the necessary arguments

### Response times and results on small data record
- Use *virtuoso.db/small.db*
- Use *elasticsearch/small*
- Evaluation results could be found in *evaluation_results/time/small* and *evaluation_results/results/small* folders

### Response times and results on large data record
- Maybe uncomment console output in the search approaches in the *SearchApproaches* folder because there are many results which are very time-consuming
- Use *virtuoso.db/large.db*
- Use *elasticsearch/large*
- Evaluation results could be found in *evaluation_results/time/large* folder

### Reponse times on base/empty project for comparison
Evaluation results could be found in *evaluation_results/time/empty* folder.

### Further Elasticsearch time measurements
- As *path* parameter you have to specify the path to the metadata_records folder of the [Sample Dataset](http://dx.doi.org/10.18154/RWTH-2020-09886)
- Evaluation results could be found in *evaluation_results/times/ES* folder
- Reindexing: Use *virtuoso.db/large.db* and *elasticsearch/large*
- Add: Use *virtuoso.db/es_9900.db* and *elasticsearch/es_9900*
- Update: Use *virtuoso.db/large.db* and *elasticsearch/large* 
- Delete: Use *virtuoso.db/large.db* and *elasticsearch/large* 
- Tests for add/update/delete are executed with additional rule for newest version specified in engmeta2 profile. If you want to run it without additional rules you have to replace named graph https://purl.org/coscine/ap/engmeta2/ by the same file (engmeta.ttl in [Sample Dataset](http://dx.doi.org/10.18154/RWTH-2020-09886)) without the additional rule.

### Generate metadata records
After execution, the generated metadata records can be found in the folder *data/engmeta2*.

### Commandline arguments

| Option        | Description | 
| ------------- |-------------| 
|-a, --action   | Required. Possible action:  small, large, empty, elasticsearch, create|
|-t, --test     | Possible tests for elasticsearch action: add, update, delete or reindex|
|-p, --path     | Specify path to metadata records for elasticsearch action|
|-n, --number   | Specify the number of metadata records to be generated for create action|

## Further notes:
In the context of the master thesis the output of the results was omitted for stopping the times. To reproduce this, the console output of the results (as well as the selection of the results) would have to be commented out in the respective approach and the project would have to be rebuilt. Defining and setting the rulesets is also commented out.

The test for measuring the validation and storage of a metadata graph in Virtuoso by CoScInE is not included in the publication due to the need for authentication. It can be reproduced by using the *StoreMetadataForFile* method in the [TreeAPI](https://git.rwth-aachen.de/coscine/api/treeapi/-/blob/master/src/Tree/Controllers/TreeController.cs) project.
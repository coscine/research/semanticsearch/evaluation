﻿using SemanticSearchEvaluation;
using System;
using System.Linq;

namespace BifContains
{
    /// <summary>
    /// Project to create exeutable for bif:contains approach.
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            var query = args[0];

            VirtuosoConnector connector = new VirtuosoConnector();

            // uses all defined literal rules, compound rules are not possible
            var results = connector.QueryWithResultSet($@"SELECT DISTINCT ?s WHERE {{
                ?s a ?profile.
                ?profile a {Uris.SH_NODE_SHAPE} .
                {{
                    SELECT ?s {{
                        ?s ?p ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""                        
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""                        
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?class {Uris.RDFS_SUBCLASS_OF}* ?superclass .
                        ?superclass {Uris.RDFS_LABEL} ?label .
                        ?o a ?class .   
                        FILTER(?superclass != <{Uris.DFG}>) .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""

                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_GIVEN_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_FAMILY_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }} 
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?membership a {Uris.ORG_MEMBERSHIP} .
                        ?membership {Uris.ORG_MEMBER} ?o .
                        ?membership {Uris.ORG_ORGANIZATION} ?organization .
                        ?organization {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?membership a {Uris.ORG_MEMBERSHIP} .
                        ?membership {Uris.ORG_MEMBER} ?o .
                        ?membership {Uris.ORG_ORGANIZATION} ?unit .
                        ?organization {Uris.ORG_HAS_UNIT} ?unit .
                        ?organization {Uris.RDFS_LABEL} ?label  .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?organization {Uris.ORG_HAS_UNIT} ?o .
                        ?organization {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.DCTERMS_TITLE} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        ?label bif:contains ""{query}""
                    }}
                }}
            }}");

            var searchResults = results.Select(x => x.Value("s").ToString()).ToList();

            foreach (var result in searchResults)
            {
                Console.WriteLine(result);
            }
        }
    }
}
﻿using SemanticSearchEvaluation;
using System;
using System.Linq;

namespace Query
{
    /// <summary>
    /// Project to create exeutable for SPARQL query execution.
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            var query = args[0];

            VirtuosoConnector connector = new VirtuosoConnector();
            var results = connector.QueryWithResultSet(query);

            var searchResults = results.Select(x => x.Value("s").ToString()).ToList();

            foreach (var result in searchResults)
            {
                Console.WriteLine(result);
            }
        }
    }
}
﻿using SemanticSearchEvaluation;
using System;
using System.Linq;

namespace FacetedSearch
{
    /// <summary>
    /// Project to create exeutable for faceted search.
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            VirtuosoConnector connector = new VirtuosoConnector();

            var query = args[0];
            var add = args[1];
            if (add != "")
            {
                add = add + " .";
            }
            // uses all defined literal rules + additional pattern (facets)
            var results = connector.QueryWithResultSet($@"SELECT DISTINCT ?s WHERE {{
                {add}
                ?s a ?profile.
                ?profile a {Uris.SH_NODE_SHAPE} .
                {{
                    SELECT ?s {{
                        ?s ?p ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?class {Uris.RDFS_SUBCLASS_OF}* ?superclass .
                        ?superclass {Uris.RDFS_LABEL} ?label .
                        ?o a ?class .   
                        FILTER(?superclass != <{Uris.DFG}>) .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_GIVEN_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.FOAF_FAMILY_NAME} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }} 
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?membership a {Uris.ORG_MEMBERSHIP} .
                        ?membership {Uris.ORG_MEMBER} ?o .
                        ?membership {Uris.ORG_ORGANIZATION} ?organization .
                        ?organization {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?membership a {Uris.ORG_MEMBERSHIP} .
                        ?membership {Uris.ORG_MEMBER} ?o .
                        ?membership {Uris.ORG_ORGANIZATION} ?unit .
                        ?organization {Uris.ORG_HAS_UNIT} ?unit .
                        ?organization {Uris.RDFS_LABEL} ?label  .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?organization {Uris.ORG_HAS_UNIT} ?o .
                        ?organization {Uris.RDFS_LABEL} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.DCTERMS_TITLE} ?label .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.QUDT_VALUE} ?value .
                        ?o {Uris.QUDT_UNIT} ?unit .
                        ?unit {Uris.RDFS_LABEL} ?unitLabel .
                        BIND(CONCAT(STR(?value), "" "", ?unitLabel) as ?label) .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
                UNION {{
                    SELECT ?s {{
                        ?s ?p ?o .
                        ?o {Uris.QUDT_VALUE} ?value .
                        ?o {Uris.QUDT_UNIT} ?unit .
                        ?unit qudt:symbol ?symbol .
                        BIND(CONCAT(STR(?value), "" "", ?symbol) as ?label) .
                        FILTER (lang(?label) = 'en' || lang(?label) = '') .
                        FILTER regex(STR(?label), ""{query}"", ""i"")
                    }}
                }}
            }}");

            var searchResults = results.Select(x => x.Value("s").ToString()).ToList();

            foreach (var result in searchResults)
            {
                Console.WriteLine(result);
            }
        }
    }
}